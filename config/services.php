<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'facebook' => [
     'client_id' => '364701808178873',
     'client_secret' => 'a16b963be2c4a97c6910e0c0cddcb638',
     'redirect' => 'http://localhost/blog/public/callback/facebook',
   ], 

    'google' => [
     'client_id' => '991567050270-sk55mldcvkgm0v9cduask291oa5iskqa.apps.googleusercontent.com',
     'client_secret' => 'X86SORn6XBv0oeooaJN1nGVz',
     'redirect' => 'http://localhost/blog/public/callback/google',
   ], 
];
