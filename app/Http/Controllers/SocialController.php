<?php
 namespace App\Http\Controllers;
 use Illuminate\Http\Request;
 use Validator,Redirect,Response,File;
 use Socialite;
 use App\User;
 class SocialController extends Controller
 {
   public function redirect($provider)
   {
       return Socialite::driver($provider)->redirect();
   }
   public function callback($provider)
   {
     $getInfo = Socialite::driver($provider)->user(); 
     $user = $this->createUser($getInfo,$provider); 
     auth()->login($user); 

     return redirect()->to('/home');
   }
   function createUser($getInfo,$provider){
   $user = User::where('provider_id', $getInfo->id)->first();
   if (!$user) {
        $user = User::create([
           'name'     => $getInfo->name,
           'email'    => $getInfo->email,
           'provider' => $provider,
           'provider_id' => $getInfo->id
       ]);
     }
     return $user;
   }


    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
      
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback()
    {
        try {
    
            $user = Socialite::driver('google')->user();
     
            $finduser = User::where('google_id', $user->id)->first();
     
            if($finduser){
     
                Auth::login($finduser);
    
                return redirect('/home');
     
            }else{
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'provider'=> $user->id,
                    'password' => encrypt('123456dummy')
                ]);
    
                Auth::login($newUser);
     
                return redirect('/home');
            }
    
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
    
 }